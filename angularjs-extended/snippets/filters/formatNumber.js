myModule.filter('formatNumber', function () {

  //Reversing given string
  function reverse(str) {
    return str.split('').reverse().join('');
  }

  //Formatting the number
  return function (number, delimiter) {
    number = number.toString();
    delimiter = reverse(delimiter || ',');
    var result = '',
        len = number.length;
    //Loop over the digits of the number
    for (var i = len - 1; i >= 0; i -= 1) {
      if ((len - 1 - i) % 3 === 0 && i !== len - 1) {
        result += delimiter;
      }
      result += number[i];
    }
    return reverse(result);
  };
});