function ToDoCtrl($scope) {
    $scope.current = '';
    $scope.todos = ['Item1', 'Item2'];
    $scope.delete = function (idx) {
        $scope.todos.splice(idx, 1);
    };
    $scope.add = function () {
        $scope.todos.push($scope.current);
        $scope.current = '';
    };
}
