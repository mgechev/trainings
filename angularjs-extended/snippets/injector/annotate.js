var $injector = angular.injector(['ng']);

function DummyCtrl($http) {
  //body
}
$injector.annotate(DummyCtrl);

DummyCtrl.$inject = ['$timeout'];
$injector.annotate(DummyCtrl);

$injector.annotate(['$browser', DummyCtrl]);