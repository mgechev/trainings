angular.module('myModule', [])
.config(function ($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'url-to-the-template',
    controller: function (Dependency1, Dependency2) {
    },
    resolve: {
      Dependency1: function ($q) {
        var deferred = $q.defer();
        //body
        return deferred.promise;
      },
      Dependency2: function ($q) {
        var deferred = $q.defer();
        //body
        return deferred.promise;
      }
    }
  })
  .otherwise({
    redirectTo: '/'
  });
});