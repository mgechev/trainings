angular.module('myModule', [])
.config(function ($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'url-to-the-template',
    controller: 'MainCtrl'
  })
  .when('/about/:id', {
    templateUrl: 'url-to-the-template',
    controller: 'AboutCtrl'
  })
  .otherwise({
    redirectTo: '/'
  });
});