describe('LoginCtrl', function () {

  var scope;
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    $controller('LoginCtrl', {
      $scope: scope,
      Req: {
        post: function () {
        }
      }
    }); 
  });

  it('should return false when the login method is called with invalid e-mail', function () {
    expect(scope.login('e-mail', 'password')).toBe(false);
  });

});