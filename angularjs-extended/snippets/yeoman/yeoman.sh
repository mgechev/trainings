#Install Yeoman
npm install -g yeoman

#Install AngularJS generator
npm install -g generator-angular

#Create project directory
mkdir angular-project
cd angular-project

#Creating AngularJS project
yo angular