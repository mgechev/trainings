myModule.animation('animate-enter', function () {
  return {
    setup : function (element) {
      element.css('opacity', 1);
      return data;
    },
    start : function(element, done, data) {
      element.fadeOut(200, function () {
        done();
      });
    }
  }
});