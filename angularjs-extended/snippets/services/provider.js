myModule.provider('Dummy', function () {
  var services = {
        type1: { foo: 'bar' },
        type2: { bar: 'foo' }
      },
      type = 'type1';
  return {
    //Method responsible for
    //returning the service
    $get: function () {
      return services[type];
    },
    configMe: function (serviceType) {
      type = serviceType;
    }
  };
})
//In cofig we are able to invoke
//only providers. We can access their
//methods which are on the level of
//the $get method
.config(function (DummyProvider) {
  DummyProvider.configMe('type2');
});