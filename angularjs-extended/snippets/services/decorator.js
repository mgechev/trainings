myModule.factory('DummyService', function () {
  return {
    foo: 'bar'
  };
})
.config(function ($provide) {
  //We are able to inject $delegate, which is local property
  //i.e. there is no global service called $delegate.
  //Through $delegate we are able to extend the service's public API.
  $provide.decorator('DummyService', function ($delegate) {
    $delegate.baz = 'foobar';
    return $delegate;
  });
});