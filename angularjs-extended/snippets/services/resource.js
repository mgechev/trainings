var users = User.query(function () {
  // body...
});

var User = $resource('/user/:userId', { userId: '@id' }),
    user = User.get({ userId:123 }, function () {
      user.name = 'Foo';
      user.$save();
    });

var user = new User({
  name: 'baz'
});

user.$save();