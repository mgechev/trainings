function factory(name, factoryFn) {
  return provider(name, { $get: factoryFn });
}

function service(name, constructor) {
  return factory(name, ['$injector', function($injector) {
    return $injector.instantiate(constructor);
  }]);
}