$provide.factory('interceptors', function(Dep1, Dep2) {
  return {
    request: function(config) {
      //we can manipulate the request configuration
    },
    requestError: function(rejection) {
      //called when the previous request have thrown error
      //or have been rejected
    },
    response: function(response) {
      //we can manipulate the response
    },
    responseError: function(rejection) {
      //we can handle error response
    };
  }
});

$httpProvider.interceptors.push('interceptors');