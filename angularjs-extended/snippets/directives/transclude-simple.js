myModule.directive('simpleContainer', function () {
  return {
    restrict: 'E',
    transclude: true,
    template: '<div>I\'m a <span ng-transclude></span></div>'
  };
});