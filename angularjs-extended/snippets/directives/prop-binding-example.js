angular.module('demo', [])
.controller('MainCtrl', function ($scope, $timeout) {
  $scope.value = 0;
})
.directive('dir', function () {
  return {
    scope: {
      //Creates two-way binding
      //between the property named
      //with the value of `scopeProp`
      //and the local property `value`
      value: '=scopeProp'
    },
    restrict: 'E',
    template: '<input type="text" ng-model="value" />'
  };
});