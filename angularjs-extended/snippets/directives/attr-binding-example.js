angular.module('demo', [])
.controller('MainCtrl', function ($scope, $timeout) {
  $scope.value = 0;
  function incrementValue() {
    $scope.value += 1;
    $timeout(incrementValue, 1000);
  }
  incrementValue();
})
.directive('dir', function () {
  return {
    scope: {
      value: '@myAttr'
    },
    restrict: 'E',
    template: '<div></div>',
    link: function (scope, el) {
      scope.$watch('value', function (v) {
        el.find('div').html(v);
      });
    }
  };
});