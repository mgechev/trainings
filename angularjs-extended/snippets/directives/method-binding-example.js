angular.module('demo', [])
.controller('MainCtrl', function ($scope, $timeout) {
  $scope.foo = function () {
    alert('Foo');
  };
})
.directive('dir', function () {
  return {
    scope: {
      method: '&scopeMethod'
    },
    restrict: 'E',
    template: '<button ng-click="method()">Call foo</button>'
  };
});