.controller('MainCtrl', function ($scope) {
  $scope.foo = function () {
    console.log('Inside foo');
  };
  $scope.bar = 'bar';
})
.directive('d1', function () {
  return {
    scope: {
      attrValue: '@attr',
      method: '&baseMethod',
      binding: '=bar'
    },
    template: '<input type="text" ng-model="binding" /><button ng-click="method()">Click me</button>',
    link: function (scope) {
      scope.$watch('attrValue', function (value) {
      console.log(scope.attrValue);
      });
    }
  };
})