myModule..directive('container', function () {

  function getElementByClass(parent, className) {
    return angular.element((parent[0] || parent).getElementsByClassName(className));
  }

  return {
    transclude: true,
    replace: true,
    restrict: 'E',
    template: '<div><div class="left"></div><div class="right"></div></div>',
    compile: function (el, attr, transcludeFn) {
      return function (scope, elem, attrs) {
        transcludeFn(scope, function (clone) {
          var left = getElementByClass(elem, 'left'),
              right = getElementByClass(elem, 'right');
          angular.forEach(clone, function (el) {
            if (el.nodeType === 1) {
              if (typeof el.getAttribute('red') === 'string') {
                left.append(el);
              } else {
                right.append(el);
              }
            }
          });
        });
      };
    }
  };
})