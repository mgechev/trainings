var SSN_REGEXP = /^\-?\d*$/;
app.directive('ssn', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (SSN_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('ssn', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('ssn', false);
          return undefined;
        }
      });
    }
  };
});