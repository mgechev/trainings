angular.element(document).ready(function () {
	angular.module('myModule', ['dependencies']);
	angular.bootstrap(document, ['myModule']);
});