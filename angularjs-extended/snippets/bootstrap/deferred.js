//By setting this property the bootstrap won't
//continue until we call .resumeBootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

angular.element(document).ready(function () {
  angular.module('myApp', []);
  angular.bootstrap(document, ['myApp']);
});

//Possible mocking...

setTimeout(function () {
  //Resuming the bootstrap
  angular.resumeBootstrap();
}, 100);