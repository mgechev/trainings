function MainCtrl($scope) {
  $scope.$broadcast('call-child', data);
  $scope.foo = function () {
    //do something
  };
}

function InnerCtrl($scope) {
  $scope.foo();
  $scope.$on('call-child', function (evnt, data) {
    /**
     * Properties of evnt
     *
     * targetScope - the scope on which the event was emitted or broadcasted
     * currentScope - the current scope handling the event
     * name - name of the event
     * stopPropagation - stop the propagation
     * preventDefault - set defaultPrevented to true
     * defaultPrevented - boolean flag
     */
  });
}