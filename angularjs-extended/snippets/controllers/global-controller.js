/**
 * Controller defined as global function
 * @param {object} $scope    The current scope
 * @param {object} $location Manipulates and returns different location parameters
 * @param {object} User      Custom service
 * @param {object} Auth      Custom service
 */
function MainCtrl($scope, $location, User, Auth) {
  if (!Auth.isLogged()) {
    $location.path('login');
  }
  $scope.name = User.getName();
}