//Specifying the dependencies in the definition
angular.module('myModule').controller('MainCtrl', ['$scope', '$location', 'User', 'Auth', function ($scope, $location, User, Auth) {
  if (!Auth.isLogged()) {
    $location.path('login');
  }
  $scope.name = User.getName();
}]);
