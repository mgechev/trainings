/**
 * Controller defined on module level.
 * @param {object} $scope    The current scope
 * @param {object} $location Manipulates and returns different location parameters
 * @param {object} User      Custom service
 * @param {object} Auth      Custom service
 */
angular.module('myModule', function ($scope, $location, User, Auth) {
  if (!Auth.isLogged()) {
    $location.path('login');
  }
  $scope.name = User.getName();
});