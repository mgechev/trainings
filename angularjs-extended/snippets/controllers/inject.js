function MainCtrl() {
  if (!Auth.isLogged()) {
    $location.path('login');
  }
  $scope.name = User.getName();
}

//Specifying the dependencies of the controller
MainCtrl.$inject = ['$scope', '$location', 'User', 'Auth'];
