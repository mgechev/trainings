angular.module('ng')
.factory('sample', function ($q, $timeout) {
  return function () {
    var p1 = $timeout(function () {
      console.log(1);
    }, 100);
    var p2 = $timeout(function () {
      console.log(2);
    }, 200);
    var p3 = $timeout(function () {
      console.log(3);
    }, 1000);
    return $q.all([p1, p2, p3]);
  };
});