var deferred = $q.defer();
//check whether the data is in the cache
if (cache[model]) {
  deferred.resolve(cache[model]);
} else {
  //make request for resolving the data
  $http.get(modelUrl)
  .then(function (data) {
    deferred.resolve(data); 
  });
}
//return promise in both cases
//we create this promise explicitly
return deferred.promise;