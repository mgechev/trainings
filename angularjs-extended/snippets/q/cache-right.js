if (cache[model]) {
  return $q.when(cache[model]); 
} else {
  return $http.get(modelUrl);
}