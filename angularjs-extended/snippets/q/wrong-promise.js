function foo() {
  var deferred = $q.defer();
  $http.get(url)
  .then(function (data) {
    data = format(data);
    deferred.resolve(data);
  }, function () {
    //body
  });
  return deferred.promise;
}

//...

foo()
.then(function (formattedData) {
});