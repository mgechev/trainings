$evalAsync: function(expr) {
  if (!$rootScope.$$phase && !$rootScope.$$asyncQueue.length) {
    $browser.defer(function() {
      if ($rootScope.$$asyncQueue.length) {
        $rootScope.$digest();
      }
    });
  }
  this.$$asyncQueue.push({scope: this, expression: expr});
}