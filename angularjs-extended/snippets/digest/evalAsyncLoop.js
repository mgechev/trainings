while(asyncQueue.length) {
  try {
    asyncTask = asyncQueue.shift();
    asyncTask.scope.$eval(asyncTask.expression);
  } catch (e) {
    $exceptionHandler(e);
  }
}